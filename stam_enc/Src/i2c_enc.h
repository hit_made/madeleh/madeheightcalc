/*
 * i2c_enc.h
 *
 *  Created on: 9 May 2019
 *      Author: Maor
 */

#ifndef I2C_ENC_H_
#define I2C_ENC_H_

#include <stdint.h>

#define DEFAULT_CAL_VALUE 11460

#define I2C_PREAMBLE 0xFEEDABBA


#define inrange(d,x,y) ((d>=x)&&(d<=y))?1:0

/*--------------i2c related--------*/
typedef struct __attribute__((packed)){
  uint32_t nPreamble;
  uint32_t nCalValue;
  union{
    struct __attribute__((packed)){
      unsigned bResetDev : 1;
      unsigned bNewCalValue :1;
      unsigned nRsvd : 6;
    };
    uint8_t nRequests;
  };
  uint8_t nSeqNum;
  uint16_t nChecksum;
} i2c_rx_msg_type;


typedef struct __attribute__((packed)) {
  uint32_t nPreamble;
  uint32_t nCurrentpos;
  uint8_t bNewPos;
  uint8_t nSeqNum;
  uint16_t nChecksum;
} i2c_tx_msg_type;



//prototype
void init_i2c_message();
uint8_t HandleI2cMsg();
void build_i2c_tx_msg();




#endif /* I2C_ENC_H_ */
