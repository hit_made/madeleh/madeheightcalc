/*
 * i2c_enc.c
 *
 *  Created on: 9 May 2019
 *      Author: Maor
 */


#include "i2c_enc.h"
#include "iir.h"

volatile i2c_rx_msg_type i2c_rx_msg;
volatile i2c_tx_msg_type i2c_tx_msg;

extern volatile IIRAvg_type sDistAvg;

void init_i2c_message()
{

    //setup premble
  i2c_tx_msg.nPreamble = I2C_PREAMBLE;
  i2c_tx_msg.nSeqNum =0;
  i2c_tx_msg.bNewPos = 0;
}

uint8_t HandleI2cMsg()
{

  //step 1: verify msg is aligned.
  if(i2c_rx_msg.nPreamble != I2C_PREAMBLE)
  {
    return 1;
  }

  //step 2: test what needs to be handled.
  if(i2c_rx_msg.bResetDev)
  {
    //reset of current values was requested.
    sDistAvg.res = 0;
    usb_print("position Reset.\n\r");
  }


  return 0;
}


uint16_t generate_checksum(uint8_t* pData,uint16_t nSize)
{
  uint16_t checksum=0;
  for(int i=0;i<nSize;i++)
  {
    checksum += *(pData+i);
  }

  return checksum;
}

void build_i2c_tx_msg()
{

  //copy current pos to msg.
  i2c_tx_msg.nCurrentpos = sDistAvg.res;

  i2c_tx_msg.nSeqNum++;

  //build tx
  i2c_tx_msg.nChecksum = generate_checksum((uint8_t*)&(i2c_tx_msg.nPreamble),sizeof(i2c_tx_msg_type)-2);

}

