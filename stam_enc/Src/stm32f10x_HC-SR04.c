#include "stm32f10x_HC-SR04.h"


extern TIM_HandleTypeDef htim2;


int HCSR04RequestDistance()
{

	(US_TIMER)->CNT = 0;


	//start the timer.
	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);
	HAL_TIM_IC_Start_IT(&htim2,TIM_CHANNEL_1|TIM_CHANNEL_2);


}


int nSolveDist(uint32_t nCCR2,uint32_t nCCR1,uint32_t* pRes)
{
	int32_t nRes;

	//solve the distance.
	nRes =  (nCCR2-nCCR1)*165/1000;

	if(nRes<0)
	{
		//drop the sample.
		return 1;
	}
	else
	{
		*pRes = nRes;
		return 0;
	}

}



int HCSR04GetDistance_blocking(int32_t* pRes) {

	int32_t nRes;

	(US_TIMER)->CNT = 0;


	//start the timer.
	HAL_TIM_Base_Start(&htim2);
	HAL_TIM_PWM_Start(&htim2,TIM_CHANNEL_3);
	HAL_TIM_IC_Start(&htim2,TIM_CHANNEL_1|TIM_CHANNEL_2);


	//wait until timer alerts that a new event was cpatured.
	while(!__HAL_TIM_GET_FLAG(&htim2,TIM_FLAG_UPDATE));


	//stop the timer.
	HAL_TIM_Base_Stop(&htim2);

	//clear the event flag.
	__HAL_TIM_CLEAR_FLAG(&htim2,TIM_FLAG_UPDATE);


	//solve the distance.
	nRes =  (US_TIMER->CCR2-US_TIMER->CCR1)*165/1000;

	if(nRes<0)
	{
		//drop the sample.
		return 1;
	}
	else
	{
		*pRes = nRes;
		return 0;
	}
}
