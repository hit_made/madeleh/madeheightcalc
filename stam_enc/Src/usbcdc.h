/*
 * usbcdc.h
 *
 *  Created on: 11 May 2019
 *      Author: Maor
 */

#ifndef USBCDC_H_
#define USBCDC_H_

#include <stdint.h>


void usb_tx_post(uint8_t* pBuf,uint8_t nLen);
void cdc_init_usbd(void);


#endif /* USBCDC_H_ */
