/*
 * iir.c
 *
 *  Created on: 25 May 2019
 *      Author: Maor
 */
#include "iir.h"
#include <stdint.h>

void InitIIRAvg(IIRAvg_type* pIirAvg, uint32_t nAlpha, uint8_t nDenominatorBits)
{
	pIirAvg->res = 0;
	pIirAvg->denominatorBits = nDenominatorBits;
	pIirAvg->denominator 	= (1 << nDenominatorBits);
	pIirAvg->alpha 			= nAlpha;
	pIirAvg->oneMinusAlpha 	= (pIirAvg->denominator - nAlpha);
}

void CalcIIRAvg(IIRAvg_type* pIirAvg, uint32_t nSample)
{
	pIirAvg->res = ((nSample*pIirAvg->alpha) + (pIirAvg->res*pIirAvg->oneMinusAlpha))>>pIirAvg->denominatorBits;
}
