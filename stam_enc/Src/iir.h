/*
 * iir.h
 *
 *  Created on: 25 May 2019
 *      Author: Maor
 */

#ifndef IIR_H_
#define IIR_H_

#include <stdint.h>


/*---------typedefs---------*/
typedef struct{
	uint32_t res;
	uint32_t alpha;
	uint32_t oneMinusAlpha;
	uint32_t denominator;
	uint8_t denominatorBits;
}IIRAvg_type;

/*--------------------------*/






/*-------IIR Functions-----------------*/
void InitIIRAvg(IIRAvg_type* pIirAvg, uint32_t nAlpha, uint8_t nDenominatorBits);
void CalcIIRAvg(IIRAvg_type* pIirAvg, uint32_t nSample);
/*-------------------------------------*/


#endif /* IIR_H_ */
